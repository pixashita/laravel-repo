<?php

namespace App\Http\Controllers\ApiControllers\Auth;

use App\User;
use App\Transformers\ApiResponse;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getResetToken(Request $request)
    {
        $this->validateEmail($request);

        $user = User::where('email', $request->input('email'))->first();
        if (!$user) {
            return ApiResponse::sendError(null, trans('passwords.user', 400));
        }

        $token = $this->broker()->createToken($user);

        return ApiResponse::sendResponse(['token' => $token, 'email sent!']);
    }
}
