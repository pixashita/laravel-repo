<?php

namespace App\Http\Controllers\ApiControllers\Auth;

use App\Transformers\ApiResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * login api
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials))
            return ApiResponse::sendError('Unauthorized', 401);

        $user = $request->user();
        $tokenResult = $user->createToken('Access Token');
        $token = $tokenResult->token;

        $token->save();

        $success['access_token'] = $tokenResult->accessToken;
        $success['name'] = $user->name;
        $success['token_type'] = 'Bearer';
        $success['expires_at'] = Carbon::parse(
            $tokenResult->token->expires_at
        )->toDateTimeString();

        return ApiResponse::sendResponse($success, 'Login Success.');
    }

    /**
     * get user api
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function user(Request $request)
    {
        return ApiResponse::sendResponse($request->user());
    }

    /**
     * logout user
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Logout Success.'
        ]);
    }


}
